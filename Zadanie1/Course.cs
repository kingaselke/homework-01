﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie1
{
   public class Course
    {
        public string CourseName;
        public string CourseLeader;
        public DateTime CourseStartDate;
        public int ThresholdPoint;
        public int Presence;
        public int NumberOfStudents;
    }
}
