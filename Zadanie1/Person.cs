﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie1
{
    class Person
    {
        public enum PersonSex
        {
            M,
            F
        }
        public long IdNumber;
        public string StudentName;
        public string StudentSurname;
        public DateTime Birthday;
        public PersonSex Sex;

    }
}
