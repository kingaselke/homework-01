﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie1
{
    public class AttendanceList
    {
        public enum PersonAttendance
        {
            obecny,
            nieobecny
        }
        public double PresenceNumber;
        public double Days;
        public PersonAttendance personAttendance;
    }
}

