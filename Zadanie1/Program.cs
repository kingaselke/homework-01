﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie1
{
    class Program
    {
        // dzienniki 
        private static Dictionary<long, Person> _people = new Dictionary<long, Person>();
        private static Dictionary<long, AttendanceList> _attendance = new Dictionary<long, AttendanceList>();
        private static Dictionary<long, Homework> _homework = new Dictionary<long, Homework>();
        private static Dictionary<string, Course> _course = new Dictionary<string, Course>();

        // metody, pilnowanie prawidlowych danych

       

        private static int GetInt(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                int value;
                if (int.TryParse(Console.ReadLine(), out value))
                    return value;
            }
        }
        

        private static DateTime GetDate(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                DateTime value;
                if (DateTime.TryParse(Console.ReadLine(), out value))
                    return value;
            }
        }



        public static void Main(string[] args)
        {

            // Dodanie informacji o kursie : 


            Course course = new Course();
            
            Console.WriteLine("Podaj nazwę kursu: ");
            course.CourseName = Console.ReadLine();
            Console.WriteLine("Podaj dane prowadzącego: ");
            course.CourseLeader = Console.ReadLine();
            course.CourseStartDate = GetDate("Podaj datę rozpoczęcia kursu ");
            course.ThresholdPoint = GetInt("Podaj próg dla prac domowych [%] ");
            course.Presence = GetInt("Podaj próg dla obecności [%] ");
            course.NumberOfStudents = GetInt("Podaj liczbę studentów ");

            _course.Add(course.CourseName, course);


            Console.WriteLine("Wybierz czynność: (AddPerson, AddPresence, AddHomework, PrintInfo, Exit)");
            string choose1 = Console.ReadLine();

            if (choose1 == "AddPerson")
            {
                AddPerson();
                bool working = true;
                while (working)
                {

                    Console.WriteLine("Wybierz czynność: (AddPresence, AddHomework, PrintInfo, Exit)");
                    string choose = Console.ReadLine();

                    switch (choose)
                    {
                        case "AddPresence":
                            AddPresence();
                            break;
                        case "PrintInfo":
                            PrintInfo();
                            break;
                        case "AddHomework":
                            AddHomework();
                            break;
                        case "Exit":
                            Exit();
                            break;
                        default:
                            Console.WriteLine("błędna instrukcja");
                            break;
                    }
                }

            }

            else if (choose1 != "AddPerson")
            {
                Console.WriteLine("Musisz podać dane studentów: ");
                AddPerson();

                bool working = true;
                while (working)
                {

                    Console.WriteLine("Wybierz czynność: (AddPresence,  AddHomework, PrintInfo, Exit)");
                    string choose = Console.ReadLine();

                    switch (choose)
                    {
                        case "AddPresence":
                            AddPresence();
                            break;
                        case "PrintInfo":
                            PrintInfo();
                            break;
                        case "AddHomework":
                            AddHomework();
                            break;
                        case "Exit":
                            Exit();
                            break;
                        default:
                            Console.WriteLine("błędna instrukcja");
                            break;
                    }
                }
            }
        }

        private static void Exit()
        {
            System.Environment.Exit(0);
        }

        private static void PrintInfo()
        {
            

                foreach (var course in _course)
            {
                Console.WriteLine("Nazwa kursu: "+ course.Value.CourseName);
                Console.WriteLine("Data rozpoczęcia kursu: "+course.Value.CourseStartDate);
                Console.WriteLine("Próg dla zadań domowych: "+course.Value.ThresholdPoint+"%");
                Console.WriteLine("Próg dla obecności "+course.Value.Presence+"%");
                Console.WriteLine("Zaliczenie na podstawie obecności: ");

                foreach (var person in _people)

                {
                    if (!_attendance.ContainsKey(person.Key))
                    {
                        Console.WriteLine("nie uzupełniłeś punktacji z prac domowcyh dla studenta: " + person.Value.StudentName + person.Value.StudentSurname);
                    }
                    else
                    {
                        string wynik;
                    if (_attendance[person.Key].PresenceNumber / _attendance[person.Key].Days * 100 > course.Value.Presence)
                        wynik = "zaliczony";
                    else
                        wynik = "niezaliczony";
                    Console.WriteLine(person.Value.StudentName + " " + person.Value.StudentSurname + " " +
                      _attendance[person.Key].PresenceNumber + "/" + _attendance[person.Key].Days +"("+ _attendance[person.Key].PresenceNumber / _attendance[person.Key].Days*100+"%)" + " " + wynik);
                        
                    }
                   

                }

                Console.WriteLine("Zaliczenie na podstawie prac domowych:");


                foreach (var person in _people)
                {
                    if (!_homework.ContainsKey(person.Key))
                    {
                        Console.WriteLine("nie uzupełniłeś dziennika dla studenta: "+ person.Value.StudentName+person.Value.StudentSurname);
                    }
                    else
                    {

                        string result;
                        if (_homework[person.Key].NumberOfPoints / _homework[person.Key].MaxNumberOfPoints * 100 >
                            course.Value.ThresholdPoint)
                            result = "zaliczony";
                        else
                            result = "niezaliczony";
                        Console.WriteLine(person.Value.StudentName + " " + person.Value.StudentSurname + " " +
                                          _homework[person.Key].NumberOfPoints + "/" +
                                          
                                          _homework[person.Key].MaxNumberOfPoints + 
                                          "("+ _homework[person.Key].NumberOfPoints / _homework[person.Key].MaxNumberOfPoints*100+"%)"+
                                          " " + result);
                    }
                }
            }

        }
        private static void AddPerson()
        {
            
                foreach (var course in _course)
                {
                    int i = 0;
                    while (i < course.Value.NumberOfStudents)
                    {
                        Person person = new Person();
                        Console.WriteLine("podaj imię");
                        person.StudentName = Console.ReadLine();
                        Console.WriteLine("podaj nazwisko");
                        person.StudentSurname = Console.ReadLine();
                        person.IdNumber = GetInt("podaj numer id");
                        if (_people.ContainsKey(person.IdNumber))
                        {
                            Console.WriteLine("nr Id juz istenieje");
                        while(_people.ContainsKey(person.IdNumber))
                            person.IdNumber = GetInt("podaj numer id");
                        }
                        person.Birthday = GetDate("podaj datę ur");

                        bool work=true;
                        while (work)
                        {
                        
                            Console.WriteLine("podaj płeć: F/M ");
                            var sex=Console.ReadLine();
                            if (sex == "M" || sex == "F")
                            {
                                person.Sex = (Person.PersonSex) Enum.Parse(typeof(Person.PersonSex), sex);
                                work = false;
                            }

                            else
                            {
                                Console.WriteLine("Błędne dane");

                            }
                        }
                        _people.Add(person.IdNumber, person);
                        i++; 
                    } 
                }
            
            
        }


        public static void AddPresence()

        {
            foreach (var person in _people)
            {
                AttendanceList attendanceList = new AttendanceList();

                
                bool work = true;
                while (work)
                {

                   // Console.WriteLine("podaj płeć: F/M ");

                    Console.WriteLine("podaj czy student " + person.Value.StudentName +
                                  " " + person.Value.StudentSurname + " był obecny/nieobecny");


                    var attendace = Console.ReadLine();
                    if (attendace == "obecny" || attendace == "nieobecny")
                    {

                        if (!_attendance.ContainsKey(person.Key))
                        {

                            if (attendace == "obecny")
                            {
                                attendanceList.PresenceNumber++;
                                attendanceList.Days++;
                                _attendance.Add(person.Key, attendanceList);
                            }
                            else if (attendace=="nieobecny")
                            {
                                attendanceList.Days++;
                                _attendance.Add(person.Key, attendanceList);
                            }
                        }
                        else
                        {
                            if (attendace == "obecny")
                            {
                                _attendance[person.Key].PresenceNumber++;
                                _attendance[person.Key].Days++;
                            }

                            else if (attendace=="nieobecny")
                            {
                                _attendance[person.Key].Days++;
                            }

                        }

                        attendanceList.personAttendance = (AttendanceList.PersonAttendance)Enum.Parse(typeof(AttendanceList.PersonAttendance), attendace);
                        work = false;
                    }

                    else
                    {
                        Console.WriteLine("Błędne dane");

                    }
                }


               

            }


        }


        private static void AddHomework()
        {

            Console.WriteLine("podaj maxymalną liczbę punktów za zadanie: ");
            int maxNumberOfPoints = int.Parse(Console.ReadLine());

            foreach (var person in _people)
            {
                Homework homework = new Homework();

                if (!_homework.ContainsKey(person.Key))
                {

                    Console.WriteLine("podaj liczbę punktów dla studenta: " + person.Value.StudentName +
                                                                            " " + person.Value.StudentSurname);
                    homework.NumberOfPoints = int.Parse(Console.ReadLine());
                    homework.MaxNumberOfPoints = maxNumberOfPoints;

                    _homework.Add(person.Key, homework);
                }

                else
                {
                    _homework[person.Key].MaxNumberOfPoints += maxNumberOfPoints;
                    
                    Console.WriteLine("podaj liczbę punktów dla studenta: " + person.Value.StudentName +
                                      " " + person.Value.StudentSurname);
                    _homework[person.Key].NumberOfPoints += int.Parse(Console.ReadLine());
                }
            }
        }
    }
}







